#!/bin/bash

#rm -rf note/migrations
sudo -u postgres psql -c 'drop database mymbd;'
sudo -u postgres psql -c 'create database mymbd;'
sudo -u postgres psql -c 'grant all privileges on database mymbd to admin;'
python manage.py makemigrations
python manage.py makemigrations core
python manage.py migrate
#python manage.py reborn
