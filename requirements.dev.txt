Django==2.0.7
django-debug-toolbar==1.9.1
django-extensions==2.1.0
psycopg2==2.7.5
pytz==2018.5
six==1.11.0
pillow<4.4.0
