from django.test import TestCase, RequestFactory
from django.urls import reverse

from core.models import Movie
from core.views import MovieList


class MovieListPagionation(TestCase):

    def setUp(self):
        for n in range(15):
            Movie.objects.create(title='Title {}'.format(n), year=1990 + n, runtime=100)

    def testFirstPage(self):
        movie_list_path = reverse('core:MovieList')
        request = RequestFactory().get(path=movie_list_path)
        response = MovieList.as_view()(request)
        self.assertEqual(200, response.status_code)
        self.assertTrue(response.context_data['is_paginated'])
