from uuid import uuid4

from django.conf import settings
from django.db import models
from django.db.models import Sum


class MovieManager(models.Manager):
    def all_with_related_persons(self):
        qs = self.get_queryset()
        qs = qs.select_related('director')
        qs = qs.prefetch_related('writers', 'actors')

        return qs

    def all_with_pers_and_score(self):
        qs = self.all_with_related_persons()
        qs = qs.annotate(score=Sum('vote__value'))
        return qs

    def top_movies(self, limit=10):
        qs = self.get_queryset()
        qs = qs.annotate(vote_sum=Sum('vote__value'))
        qs = qs.exclude(vote_sum=None)
        qs = qs.order_by('-vote_sum')
        qs = qs[:limit]
        return qs


class Movie(models.Model):
    NOT_RATED = 0
    RATED_G = 1
    RATED_PG = 2
    RATED_R = 3

    RATINGS = (
        (NOT_RATED, 'NR - Not Rated'),
        (RATED_G, 'G - General Audiences'),
        (RATED_PG, 'Pg - Parental Guidance'),
        (RATED_R, 'R - Restricted'),
    )

    objects = MovieManager()

    title = models.CharField(max_length=140)
    plot = models.TextField()
    year = models.PositiveIntegerField()
    rating = models.IntegerField(choices=RATINGS, default=NOT_RATED)
    runtime = models.PositiveIntegerField()
    website = models.URLField(blank=True)

    class Meta:
        ordering = ('-year', 'title')

    director = models.ForeignKey(to='Person',
                                 on_delete=models.SET_NULL,
                                 related_name='directed',
                                 null=True, blank=True)

    writers = models.ForeignKey(to='Person',
                                related_name='writing_credits',
                                on_delete=models.SET_NULL,
                                null=True, blank=True)

    actors = models.ManyToManyField(to='Person',
                                    through='Role',
                                    related_name='acting_credits',
                                    blank=True)

    def __str__(self):
        return '{} ({})'.format(self.title, self.year)


class PersonManager(models.Manager):
    def all_with_prefetch_movies(self):
        qs = self.get_queryset()
        return qs.prefetch_related('directed', 'writing_credits', 'role_set__movie')


class Person(models.Model):
    first_name = models.CharField(max_length=140)
    last_name = models.CharField(max_length=140)
    born = models.DateField()
    died = models.DateField(null=True, blank=True)

    objects = PersonManager()

    class Meta:
        ordering = ('last_name', 'first_name')

    def __str__(self):
        base = '{}, {}'.format(self.last_name, self.first_name)
        if self.died:
            return base + '({} - {})'.format(self.born, self.died)
        else:
            return base + '({})'.format(self.born)


class Role(models.Model):
    movie = models.ForeignKey(Movie, on_delete=models.DO_NOTHING)
    person = models.ForeignKey(Person, on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=140)

    class Meta:
        unique_together = ('movie', 'person', 'name')

    def __str__(self):
        return '{} {} {}'.format(self.movie_id, self.person_id, self.name)


class VoteManager(models.Manager):
    @staticmethod
    def get_vote_or_blank_vote(movie, user):
        try:
            return Vote.objects.get(movie=movie, user=user)
        except Vote.DoesNotExist:
            return Vote(movie=movie, user=user)


class Vote(models.Model):
    UP = 1
    DOWN = -1

    VALUE_CHOICES = (
        (UP, 'Like'),
        (DOWN, 'Dislike'),
    )

    objects = VoteManager()

    value = models.SmallIntegerField(choices=VALUE_CHOICES)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    voted_on = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('user', 'movie')


def movie_directory_path(instance, filename):
    return '{}/{}'.format(instance.movie_id, uuid4())


class MovieImage(models.Model):
    image = models.ImageField(upload_to=movie_directory_path)
    uploaded = models.DateTimeField(auto_now_add=True)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
